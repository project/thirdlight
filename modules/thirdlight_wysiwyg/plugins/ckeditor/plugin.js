// Register the plugin within the editor.
CKEDITOR.plugins.add('thirdlight_wysiwyg', {
  icons: 'thirdlight_wysiwyg',
  init: function(editor) {
    var config = editor.config;

    if (!Drupal.settings.thirdlight_config) {
      return false;
    }

    var thirdlight_config = Drupal.settings["thirdlight_config"];

    // check that the browser has been configured
    if (!thirdlight_config.configured) {
      editor.ui.addButton('thirdlight_wysiwyg', {
        label: Drupal.t('Third Light (disabled)'),
        command: 'thirdlight_wysiwyg'
      });

      editor.addCommand('thirdlight_wysiwyg', {
        exec: function() {
          if (thirdlight_config.reason) {
            alert(thirdlight_config.reason);
          } else {
            alert('The Third Light Browser has not been configured yet');
          }
          return;
        }
      });
      return;
    }

    // Register the toolbar buttons.
    editor.ui.addButton('thirdlight_wysiwyg', {
      label: Drupal.t('Third Light'),
      command: 'thirdlight_wysiwyg'
    });

    // command to launch the browser
    editor.addCommand('thirdlight_wysiwyg', {
      exec: function() {

        var app = new IMS.IframeAppOverlay(thirdlight_config.browserUrl, {
          options: thirdlight_config.options
        });

        // update the image
        app.on("cropChosen", function(cropDetails) {
          var img = editor.document.createElement('img');
          img.setAttribute('src', cropDetails.urlDetails.url);
          img.setAttribute('width', cropDetails.urlDetails.width);
          img.setAttribute('height', cropDetails.urlDetails.height);

          if (cropDetails.cropClass && thirdlight_config.options.cropClasses) {
            jQuery.each(thirdlight_config.options.cropClasses, function(index, curClass) {
              if (curClass.key == cropDetails.cropClass) {
                if (curClass.className) {
                  img.setAttribute("class", curClass.className);
                }
                return false;
              }
            });
          }

          if (!!cropDetails.metadata['headline']) {
            img.setAttribute("alt", cropDetails.metadata['headline'].substring(0, 256).replace(/(<([^>]+)>)/ig, "").replace(/(\r\n|\n|\r)/gm, "") || "");
          }

          if (!!thirdlight_config.titleMode && !!cropDetails.metadata[thirdlight_config.titleMode]) {
            img.setAttribute("title", cropDetails.metadata[thirdlight_config.titleMode].substring(0, 256).replace(/(<([^>]+)>)/ig, "").replace(/(\r\n|\n|\r)/gm, "") || "");
          }

          if (!!cropDetails.metadata['credit']) {
            img.setAttribute("data-credit", cropDetails.metadata['credit'].substring(0, 256).replace(/(<([^>]+)>)/ig, "").replace(/(\r\n|\n|\r)/gm, "") || "");
          }

          editor.insertElement(img);
        });
      }

    });
  }
});
