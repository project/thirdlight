<?php

/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function thirdlight_field_field_info() {
  return array(
    'thirdlight_field' => array(
      'label' => t('Thirdlight'),
      'description' => t('Creates a Thirdlight field.'),
      'default_widget' => 'thirdlight_field_widget',
      'default_formatter' => 'thirdlight_field_format',
      'default_token_formatter' => 'thirdlight_field_format_url'
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 *
 */
function thirdlight_field_field_widget_info() {
  return array(
    'thirdlight_field_widget' => array(
      'label' => t('Thirdlight'),
      'field types' => array('thirdlight_field'),
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 *
 */
function thirdlight_field_field_formatter_info() {
  return array(
    'thirdlight_field_format' => array(
      'label' => t('Thirdlight Rendered Image'),
      'field types' => array('thirdlight_field'),
    ),
    'thirdlight_field_format_url' => array(
      'label' => t('Thirdlight Image URL'),
      'field types' => array('thirdlight_field'),
      'settings'  => array(
        'delta' => '1',
      ),
    ),
    'thirdlight_field_format_caption' => array(
      'label' => t('Thirdlight Caption'),
      'field types' => array('thirdlight_field'),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function thirdlight_field_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  if ($display['type'] == 'thirdlight_field_format_url') {
    $settings = $display['settings'];
    $summary = t('Use @delta to select the delta of the thirdlight field.', array(
      '@delta' => $settings['delta'],
    )); // we use t() for translation and placeholders to guard against attacks
    return $summary;
  }
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function thirdlight_field_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];

  if ($display['type'] == 'thirdlight_field_format_url') {
    //This gets the actual settings
    $settings = $display['settings'];
    //Initialize the element variable
    $element = array();
    //Add your select box
    $element['delta'] = array(
      '#type' => 'select',
      // Use a select box widget
      '#title' => t('Delta'),
      // Widget label
      '#description' => t('Default delta for thirdlight images.'),
      // Helper text
      '#default_value' => $settings['delta'],
      // Get the value if it's already been set
      '#options' => drupal_map_assoc(range(1, 50)),
    );
    return $element;
  }
}

/**
 * Implements hook_theme().
 *
 */
function thirdlight_field_theme() {
  return array(
    'thirdlight_field_image' => array(
      'variables' => array(
        'image' => array(),
        'ignore_dimentions' => FALSE,
        'hidden' => FALSE
      )
    )
  );
}

/**
 * Theme callback function.
 *
 * @param type $vars
 * @return string
 */
function theme_thirdlight_field_image($vars) {
  $image = $vars['image'];
  $ignore_dimentions = $vars['ignore_dimentions'];
  $hidden = $vars['hidden'];

  if (!is_array($image)) {
    return '<img height="100" src="" style="display:none;" class="thirdlight-image" />';
  }

  $output = '<img';
  if (isset($image['thirdlight_field_url'])) {
    $output .= ' src="' . $image['thirdlight_field_url'] . '"';
  }

  if (isset($image['thirdlight_field_title']) && $image['thirdlight_field_title'] != '') {
    $output .= ' title="' . $image['thirdlight_field_title'] . '"';
  }

  if (isset($image['thirdlight_field_alt']) && $image['thirdlight_field_alt'] != '') {
    $output .= ' alt="' . $image['thirdlight_field_alt'] . '"';
  }

  if ($ignore_dimentions === FALSE && isset($image['thirdlight_field_width']) && $image['thirdlight_field_width'] != '') {
    $output .= ' width="' . $image['thirdlight_field_width'] . '"';
  }

  if ($ignore_dimentions === FALSE && isset($image['thirdlight_field_height']) && $image['thirdlight_field_height'] != '') {
    $output .= ' height="' . $image['thirdlight_field_height'] . '"';
  } elseif (isset($image['thirdlight_field_width']) && isset($image['thirdlight_field_height']) && $image['thirdlight_field_width'] < $image['thirdlight_field_height']) {
    $output .= ' width="100"';
  }

  if ($hidden == TRUE) {
    $output .= ' style="display:none;"';
  }

  $output .= ' class="thirdlight-image"';

  $output .= ' />';

  return $output;
}

/**
 * Implements hook_field_formatter_view().
 *
 */
function thirdlight_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    // This formatter creates a thirdlight image field selection button.
    case 'thirdlight_field_format':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          'thirdlight' => $item,
          '#markup' => theme('thirdlight_field_image', array('image' => $item))
        );
      }
      break;
    case 'thirdlight_field_format_url':
      if (isset($display['settings']['delta']) && is_numeric($display['settings']['delta'])) {
        // If the delta has been set then use this.
        $delta = $display['settings']['delta'] - 1;
        $element[] = array(
          'thirdlight' => $items[$delta],
          '#markup' => $items[$delta]['thirdlight_field_url']
        );
      }
      else {
        // Otherwise grab the list as an array.
        foreach ($items as $delta => $item) {
          $element[$delta] = array(
            'thirdlight' => $item,
            '#markup' => $item['thirdlight_field_url']
          );
        }
      }
      break;
    case 'thirdlight_field_format_caption':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          'thirdlight' => $item,
          '#markup' => $item['thirdlight_field_caption']
        );
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_field_widget_form().
 *
 * @param type $form
 * @param type $form_state
 * @param type $field
 * @param type $instance
 * @param type $langcode
 * @param type $items
 * @param type $delta
 * @param type $element
 * @return string
 */
function thirdlight_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  thirdlight_load_external_js_libraries();
  thirdlight_load_settings_js();

  $widget = $element;
  $widget['#delta'] = $delta;

  thirdlight_load_external_js_libraries();

  switch ($instance['widget']['type']) {
    case 'thirdlight_field_widget':

      if (isset($form_state['values'][$field['field_name']][$widget['#language']][$delta])) {
        $fieldset_values = $form_state['values'][$field['field_name']][$widget['#language']][$delta];
      } elseif (isset($items[$delta])) {
        $fieldset_values = $items[$delta];
      } else {
        $fieldset_values = array();
      }

      if (isset($fieldset_values['thirdlight_field']['thirdlight_fieldset'])) {
        $fieldset_values = $fieldset_values['thirdlight_field']['thirdlight_fieldset'];
      }

      if (isset($fieldset_values['thirdlight_field_browse'])) {
        // If the Browse/Choose and Delete buttons are in our field values list then remove it.
        unset($fieldset_values['thirdlight_field_browse']);
        unset($fieldset_values['thirdlight_field_delete']);
      }

			if ($field['cardinality'] != 1) {
	      $thirdlight_form['thirdlight_fieldset'] = array(
	        '#type' => 'fieldset',
	        '#title' => 'Image ' . ($delta + 1),
	      );
			}
			else {
	      $thirdlight_form['thirdlight_fieldset'] = array(
	        '#type' => 'fieldset',
	        '#title' => $instance['label'],
	      );
			}

      $thirdlight_inner_form = array();

      $thirdlight_inner_form['thirdlight_field_col_1'] = array(
        '#markup' => '<div class="thirdlight-col-image">'
      );

      if (thirdlight_field_squash_elements($fieldset_values) != '') {
        $thirdlight_inner_form['thirdlight_field_preview'] = array(
          '#markup' => '<div class="thirdlight-image-preview">' . theme('thirdlight_field_image', array('image' => $fieldset_values, 'ignore_dimentions' => TRUE)) . '</div>'
        );
      } else {
        $thirdlight_inner_form['thirdlight_field_preview'] = array(
          '#markup' => '<div class="thirdlight-image-preview">' . theme('thirdlight_field_image', array('image' => $fieldset_values, 'ignore_dimentions' => TRUE, 'hidden' => TRUE)) . '</div>'
        );
      }

      $thirdlight_inner_form['thirdlight_field_browse'] = array(
        '#type' => 'button',
        '#value' => (thirdlight_field_squash_elements($fieldset_values) == '') ? 'Browse' : 'Change',
        '#attributes' => array(
          'class' => array('thirdlight-field-button')
        )
      );

      if (thirdlight_field_squash_elements($fieldset_values) != '') {
        $thirdlight_inner_form['thirdlight_field_delete'] = array(
          '#type' => 'button',
          '#value' => 'Delete',
          '#attributes' => array(
            'class' => array('thirdlight-field-delete')
          )
        );
      }

      $thirdlight_inner_form['thirdlight_field_col_2'] = array(
        '#markup' => '</div><div class="thirdlight-col-fields">'
      );

      $thirdlight_inner_form['thirdlight_field_url'] = array(
        '#type' => 'textfield',
        '#title' => 'URL',
        '#default_value' => isset($fieldset_values['thirdlight_field_url']) ? $fieldset_values['thirdlight_field_url'] : '',
        '#attributes' => array(
          'class' => array('thirdlight-field-url')
        ),
        '#maxlength' => 255
      );

      $thirdlight_inner_form['thirdlight_field_title'] = array(
        '#type' => 'textfield',
        '#title' => 'Title',
        '#default_value' => isset($fieldset_values['thirdlight_field_title']) ? $fieldset_values['thirdlight_field_title'] : '',
        '#attributes' => array(
          'class' => array('thirdlight-field-title')
        ),
        '#maxlength' => 255
      );

      $thirdlight_inner_form['thirdlight_field_alt'] = array(
        '#type' => 'textfield',
        '#title' => 'Alt',
        '#default_value' => isset($fieldset_values['thirdlight_field_alt']) ? $fieldset_values['thirdlight_field_alt'] : '',
        '#attributes' => array(
          'class' => array('thirdlight-field-alt')
        ),
        '#maxlength' => 255
      );

      $thirdlight_inner_form['thirdlight_field_width'] = array(
        '#type' => 'textfield',
        '#title' => 'Width',
        '#default_value' => isset($fieldset_values['thirdlight_field_width']) ? $fieldset_values['thirdlight_field_width'] : '',
        '#attributes' => array(
          'class' => array('thirdlight-field-width'),
					'readonly' => 'readonly'
        ),
        '#maxlength' => 5
      );

      $thirdlight_inner_form['thirdlight_field_height'] = array(
        '#type' => 'textfield',
        '#title' => 'Height',
        '#default_value' => isset($fieldset_values['thirdlight_field_height']) ? $fieldset_values['thirdlight_field_height'] : '',
        '#attributes' => array(
          'class' => array('thirdlight-field-height'),
					'readonly' => 'readonly'
        ),
        '#maxlength' => 5
      );

      $thirdlight_inner_form['thirdlight_field_credit'] = array(
        '#type' => 'textfield',
        '#title' => 'Credit',
        '#default_value' => isset($fieldset_values['thirdlight_field_credit']) ? $fieldset_values['thirdlight_field_credit'] : '',
        '#attributes' => array(
          'class' => array('thirdlight-field-credit')
        ),
        '#maxlength' => 255
      );
			
			$tags_type = ($field['cardinality'] != 1)? 'textfield' : 'hidden';
			
      $thirdlight_inner_form['thirdlight_field_tags'] = array(
        '#type' => $tags_type,
        '#title' => 'Tags',
        '#default_value' => isset($fieldset_values['thirdlight_field_tags']) ? $fieldset_values['thirdlight_field_tags'] : '',
        '#attributes' => array(
          'class' => array('thirdlight-field-tags'),
					'placeholder' => t('Eg. Red Carpet, Ceremony, Nominees'),
        ),
        '#maxlength' => 255
      );

      $thirdlight_inner_form['thirdlight_field_col_2_end'] = array(
        '#markup' => '</div>'
      );

      $thirdlight_form['thirdlight_fieldset'] += $thirdlight_inner_form;

      $widget += $thirdlight_form;

      $widget['#attached'] = array(
        'js' => array(
          drupal_get_path('module', 'thirdlight_field') . "/js/thirdlight.js"
        )
      );

      $element['thirdlight_field'] = $widget;
      break;
  }

  return $element;
}

/**
 *
 *
 * @param type $entity_type
 * @param type $entity
 * @param type $field
 * @param type $instance
 * @param type $langcode
 * @param type $items
 * @param type $errors
 */
function thirdlight_field_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  /*
    Not used currently.
   */
}

/**
 * Implode the values of an array into a single string. Used to see if the field is empty.
 *
 * @param type $elements
 * @return string
 */
function thirdlight_field_squash_elements($elements) {
  if (!is_array($elements)) {
    return '';
  }

  return trim(implode('', array_values($elements)));
}

/**
 * Implements hook_field_is_empty().
 */
function thirdlight_field_field_is_empty($item, $field) {
  // Get the elements.
  $elements = $item['thirdlight_field']['thirdlight_fieldset'];

  // Remove the buttons from our analysis.
  unset($elements['thirdlight_field_browse']);
  unset($elements['thirdlight_field_delete']);

  // Combine all the fields into a single string, and trim just in case.
  $value = thirdlight_field_squash_elements($elements);

  // Is string empty.
  return $value == '';
}

/**
 * Implements hook_field_presave().
 */
function thirdlight_field_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {

  if ($field['type'] == 'thirdlight_field') {
    if (count($items) == 0) {
      $items = array(
        array(
          'thirdlight_field_url' => ''
        )
      );
    }
    else {
      foreach ($items as $delta => &$item) {
        $new_item = array();

        if (isset($item['thirdlight_field']['thirdlight_fieldset']['thirdlight_field_url'])) {
          $new_item['thirdlight_field_url'] = $item['thirdlight_field']['thirdlight_fieldset']['thirdlight_field_url'];
          $new_item['thirdlight_field_title'] = $item['thirdlight_field']['thirdlight_fieldset']['thirdlight_field_title'];
          $new_item['thirdlight_field_alt'] = $item['thirdlight_field']['thirdlight_fieldset']['thirdlight_field_alt'];
          $new_item['thirdlight_field_width'] = $item['thirdlight_field']['thirdlight_fieldset']['thirdlight_field_width'];
          $new_item['thirdlight_field_height'] = $item['thirdlight_field']['thirdlight_fieldset']['thirdlight_field_height'];
          $new_item['thirdlight_field_tags'] = $item['thirdlight_field']['thirdlight_fieldset']['thirdlight_field_tags'];
          $new_item['thirdlight_field_credit'] = $item['thirdlight_field']['thirdlight_fieldset']['thirdlight_field_credit'];

          $item = $new_item;
        }
        elseif (isset($item['thirdlight_field_url'])) {
          $new_item['thirdlight_field_url'] = $item['thirdlight_field_url'];
          $new_item['thirdlight_field_title'] = $item['thirdlight_field_title'];
          $new_item['thirdlight_field_alt'] = $item['thirdlight_field_alt'];
          $new_item['thirdlight_field_width'] = $item['thirdlight_field_width'];
          $new_item['thirdlight_field_height'] = $item['thirdlight_field_height'];
          $new_item['thirdlight_field_tags'] = $item['thirdlight_field_tags'];
          $new_item['thirdlight_field_credit'] = $item['thirdlight_field_credit'];

          $item = $new_item;
        }

      }
    }
  }
}

/**
 * Implements hook_init().
 */
function thirdlight_field_init() {
  global $theme;

  // Inject some CSS overrides for the core Seven theme
  if ($theme == 'seven') {
    drupal_add_css(drupal_get_path('module', 'thirdlight_field') . '/thirdlight_field_admin.css');
  }
}
