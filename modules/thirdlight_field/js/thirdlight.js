var elbutton = '';
(function($) {
  $(document).ready(function() {

    function thirdlight_clense_string(dirtystring) {
      if (dirtystring == '') {
        return '';
      }

      dirtystring = dirtystring.substring(0, 254).replace(/(<([^>]+)>)/ig, "").replace(/(\r\n|\n|\r)/gm, "");

      return dirtystring || '';
    }

    $(".thirdlight-field-delete").click(function(event) {
      event.stopPropagation();
      event.preventDefault();

      elbutton = event.currentTarget;

      // Blank all of the fields.
      $(elbutton).parent().parent().find('input.thirdlight-field-url').val('');
      $(elbutton).parent().parent().find('input.thirdlight-field-title').val('');
      $(elbutton).parent().parent().find('input.thirdlight-field-alt').val('');
      $(elbutton).parent().parent().find('input.thirdlight-field-width').val('');
      $(elbutton).parent().parent().find('input.thirdlight-field-height').val('');
      $(elbutton).parent().parent().find('input.thirdlight-field-credit').val('');

      // Remove the image and hide the field.
      $(elbutton).parent().parent().find('.thirdlight-image').attr('style', 'display:none');
      $(elbutton).parent().parent().find('.thirdlight-image').attr('src', '');

      // Hide the delete button.
      $(elbutton).hide();
    });

    $(".thirdlight-field-button").click(function(event) {

      if (event.screenX == 0 && event.screenY == 0) {
        // Prevent chrome from interpreting enter as a click.
        return false;
      }

      event.stopPropagation();
      event.preventDefault();

      elbutton = event.currentTarget;
      var elurl = $(elbutton).parent().parent().find('input.thirdlight-field-url');
      var eltitle = $(elbutton).parent().parent().find('thirdlight-field-title');
      var elalt = $(elbutton).parent().parent().find('thirdlight-field-alt');
      var elwidth = $(elbutton).parent().parent().find('thirdlight-field-width');
      var elheight = $(elbutton).parent().parent().find('thirdlight-field-height');

      var thirdlight_config = Drupal.settings["thirdlight_config"];

      if (!thirdlight_config.configured) {
        if (thirdlight_config.reason) {
          alert(thirdlight_config.reason);
        } else {
          alert('The Third Light browser has not been configured yet');
        }
        return;
      }

      var app = new IMS.IframeAppOverlay(thirdlight_config.browserUrl, {
        options: thirdlight_config.options
      });

      app.on("cropChosen", function(cropDetails) {

        $(elbutton).parent().parent().find('input.thirdlight-field-url').val(cropDetails.urlDetails.url);
        $(elbutton).parent().parent().find('input.thirdlight-field-width').val(cropDetails.urlDetails.width);
        $(elbutton).parent().parent().find('input.thirdlight-field-height').val(cropDetails.urlDetails.height);

        if (thirdlight_config.titleMode && cropDetails.metadata) {
          if (cropDetails.metadata[thirdlight_config.titleMode] !== undefined) {
            $(elbutton).parent().parent().find('input.thirdlight-field-title').val(thirdlight_clense_string(cropDetails.metadata[thirdlight_config.titleMode]));
          }

          if (cropDetails.metadata['headline'] !== undefined) {
            $(elbutton).parent().parent().find('input.thirdlight-field-alt').val(thirdlight_clense_string(cropDetails.metadata['headline']));
          }

          if (cropDetails.metadata[thirdlight_config.titleMode] !== undefined) {
            $(elbutton).parent().parent().find('.thirdlight-image').attr('title', thirdlight_clense_string(cropDetails.metadata[thirdlight_config.titleMode]));
          }

          if (cropDetails.metadata['headline'] !== undefined) {
            $(elbutton).parent().parent().find('.thirdlight-image').attr('alt', thirdlight_clense_string(cropDetails.metadata['headline']));
          }
        }

        if (cropDetails.metadata['credit'] !== undefined) {
          $(elbutton).parent().parent().find('input.thirdlight-field-credit').val(thirdlight_clense_string(cropDetails.metadata['credit']));
        }

        $(elbutton).parent().parent().find('.thirdlight-image').attr('style', '');

        $(elbutton).parent().parent().find('.thirdlight-image').attr('src', cropDetails.urlDetails.url);
      });
    });
  });
})(jQuery);
