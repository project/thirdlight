<?php

/**
 * @file
 * Stores all of the admin interface functions for the Thirdlight integration module.
 */

/**
 * The main settings form for the Third Light module.
 *
 * @param array $form The form.
 * @param array $form_state The current form state.
 *
 * @return array The form.
 */
function thirdlight_settings_form($form, &$form_state) {
  $arrSettings = thirdlight_load_settings();

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Third Light Site'),
    '#default_value' => empty($arrSettings['thirdlight_url']) ? '' : $arrSettings['thirdlight_url'],
    '#required' => TRUE,
    '#description' => t('The URL of your Third Light IMS site - e.g. http://imsdemonstration.thirdlight.com/')
  );

  $form['revisions'] = array(
    '#type' => 'select',
    '#title' => t('Show file revisions'),
    '#default_value' => empty($arrSettings['thirdlight_revisions']) ? '0' : '1',
    '#options' => array(
      "0" => t("No"),
      "1" => t("Yes")
    ),
    '#description' => t('Third Light sites support version control. Enable this to permit access to versions other than the currently active one.')
  );

  $form['metadata'] = array(
    '#type' => 'select',
    '#title' => t('Display metadata'),
    '#default_value' => empty($arrSettings['thirdlight_metadata']) ? '0' : '1',
    '#options' => array(
      "0" => t("No"),
      "1" => t("Yes")
    ),
    '#description' => t('Third Light sites support rich metadata. Enable this to offer metadata to users of the Third Light Browser.')
  );

  $theme_options = array(
    "light" => t("Light"),
    "dark" => t("Dark"),
    "blue" => t("Blue")
  );

  $form['theme'] = array(
    '#type' => 'select',
    '#title' => t('Choose a skin'),
    '#default_value' => empty($arrSettings['thirdlight_theme']) ? "" : $arrSettings['thirdlight_theme'],
    '#options' => $theme_options,
  );

  $title_options = array(
    "" => t("None"),
    "caption" => t("Caption"),
    "copyright" => t("Copyright Notice"),
    "instructions" => t("Special Instructions")
  );

  $form['titlemode'] = array(
    '#type' => 'select',
    '#title' => t('Title for inserted images'),
    '#default_value' => empty($arrSettings['thirdlight_titlemode']) ? "" : $arrSettings["thirdlight_titlemode"],
    '#options' => $title_options,
    '#description' => t('This allows you to set the title and alt text of images inserted based on the metadata set in your Third Light IMS site.')
  );

  $form['actions'] = array(
    '#tree' => FALSE,
    '#type' => 'actions'
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/content/thirdlight',
  );

  return $form;
}

/**
 * Validate the main settings form.
 *
 * @param array $form The form.
 * @param array $form_state The current form state.
 *
 * @return array The form.
 */
function thirdlight_settings_form_validate($form, &$form_state) {

  $data = $form_state['values'];

  $arrURL = parse_url($data["url"]);

  if (empty($arrURL) || !preg_match("|^https?://|", $data["url"])) {
    form_set_error("url", t('The URL provided does not appear to be valid.'));
  }

  $test_url = $data["url"] . "apps/cmsbrowser/index.html";

  // Test that the URL provided works.
  $returned_data = drupal_http_request($test_url);

  if ($returned_data->code == 200) {
    if (strpos($returned_data->data, "thirdlight_application") === FALSE) {
      form_set_error("url", t('The URL provided does not be a Third Light site supporting the Third Light Browser.'));
    }
  } else {
    form_set_error("url", t('The URL provided does not be a Third Light site supporting the Third Light Browser. Status: ' . $returned_data->status_message));
  }

  /*
    Currently no way to test this stuff.
    if (!empty($data["apikey"])) {*
    $keyValid = false;
    module_load_include('php', 'thirdlight_integration', 'inc/ImsApiClient');
    try {
    $client = new IMSApiClient($data["url"], $data["apikey"]);
    $keyValid = true;
    $globalSFFConfig = $client->Config_CheckFeatureAvailable(array("featureKey" => "SECURE_FILE_FETCH_PERMANENT"));
    if (!$globalSFFConfig) {
    form_set_error("url", t('The Third Light site does not have permanent Secure File Fetch enabled - this is required by the Third Light Browser.'));
    }
    $globalRevConfig = $client->Config_CheckFeatureAvailable(array("featureKey" => "REVISIONS"));
    if (!$globalRevConfig && !empty($data["revisions"])) {
    form_set_error("revisions", t('Version control is disabled on the Third Light site.'));
    }
    } catch (IMSApiActionException $e) {
    form_set_error("apikey", t('The API key was rejected by the Third Light IMS server.'));
    } catch (IMSApiPrerequisiteException $e) {
    form_set_error("apikey", t("API client prerequisite missing: ") . $e->getMessage());
    } catch (IMSApiClientException $e) {
    form_set_error("url", t('The URL provided does not appear to be a Third Light site.'));
    }
    }

    if (!empty($data["autologin"]) && !$keyValid) {
    form_set_error("autologin", t('Automatic log in requires that the API key be configured correctly.'));
    } */
}

/**
 * Submission function for the main settings form.
 *
 * @param array $form The form.
 * @param array $form_state The current form state.
 *
 * @return array The form.
 */
function thirdlight_settings_form_submit($form, &$form_state) {

  $data = $form_state['values'];

  variable_set('thirdilght_url', $data['url']);
  variable_set('thirdilght_theme', $data['theme']);
  variable_set('thirdilght_revisions', $data['revisions']);
  variable_set('thirdilght_metadata', $data['metadata']);
  variable_set('thirdilght_titlemode', $data['titlemode']);
  variable_set('thirdilght_apikey', isset($data['apikey']) ? $data['apikey'] : '');
  variable_set('thirdilght_autologin', isset($data['autologin']) ? $data['autologin'] : '');

  drupal_set_message(t('The Third Light integration settings were saved.'));
  $form_state['redirect'] = 'admin/config/content/thirdlight';
}

/**
 * Callback.
 *
 * @return string The page content.
 */
function thirdlight_image_formats($format_id = FALSE) {
  $output = '';

  if ($format_id !== FALSE) {
    $form = drupal_get_form('thirdlight_image_format_form', $format_id);
    $output .= drupal_render($form);
  } else {
    $output .= thirdlight_image_format_list();
  }

  return $output;
}

/**
 * Callback
 *
 * @return string The page content.
 */
function thirdlight_image_formats_add() {
  $output = '';
  $form = drupal_get_form('thirdlight_image_format_form');
  $output .= drupal_render($form);
  return $output;
}

/**
 *
 * @return string
 */
function thirdlight_image_format_list() {
  $output = '';

  $formats = thirdlight_load_formats();

  foreach ($formats as $thisVariant) {
    $variantRows[] = array(
      array('data' => $thisVariant["name"]),
      array('data' => $thisVariant["format"] . " &ndash; " . $thisVariant["width"] . " &#10005; " . $thisVariant["height"]),
      array('data' => l(t('edit'), 'admin/config/content/thirdlight/fields/' . $thisVariant["fid"]) . ' ' . l(t('delete'), 'admin/config/content/thirdlight/fields/delete/' . $thisVariant["fid"])),
    );
  }

  $output .= "<h3>" . t("Image Formats") . "</h3>";
  $output .= theme('table', array("header" => array(t('Name'), t('Details'), t('Operations')), "rows" => $variantRows));
  $output .= l(t('Add output format'), 'admin/config/content/thirdlight/fields/add', array('attributes' => array('class' => 'button')));
  return $output;
}

/**
 *
 * @param type $form
 * @param type $form_state
 * @param type $format_id
 * @return type
 */
function thirdlight_image_format_form($form, &$form_state, $format_id = FALSE) {

  if ($format_id === FALSE) {
    $format = array(
      "name" => "",
      "width" => "",
      "height" => "",
      "format" => "",
      "className" => "",
    );
  } else {
    $format = thirdlight_load_formats($format_id);

    if (!isset($format[0])) {
      drupal_set_message(t('The output format could not be found.'), 'error');
      drupal_goto('admin/config/content/thirdlight/fields');
    }

    $format = array_pop($format);
  }

  if (isset($format['fid'])) {
    $form['fid'] = array(
      '#type' => 'value',
      '#value' => $format["fid"],
    );
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $format["name"],
    '#required' => true
  );

  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => $format["width"],
    '#required' => true,
    '#description' => t('Width of the image, in pixels')
  );

  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => $format["height"],
    '#required' => true,
    '#description' => t('Height of the image, in pixels')
  );

  $form['format'] = array(
    '#type' => 'select',
    '#title' => t('Format'),
    '#default_value' => $format["format"],
    '#options' => drupal_map_assoc(array("JPG", "PNG", "GIF")),
    '#description' => t('File format for the image')
  );

  $form['className'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS Class'),
    '#default_value' => $format["className"],
    '#description' => t('An optional CSS class to apply to images inserted using this category.')
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/content/thirdlight/fields',
  );

  return $form;
}

/**
 *
 * @param type $form
 * @param type $form_state
 */
function thirdlight_image_format_form_validate($form, &$form_state) {
  $data = $form_state['values'];

  $formats = thirdlight_load_formats();

  foreach ($formats as $format) {
    if (0 == strcasecmp($format["name"], $data["name"]) && $format["fid"] != $data['fid']) {
      form_set_error("name", t('An output format already exists with that name'));
      break;
    }
  }

  foreach (array("width", "height") as $dimension) {
    $intDim = intval($data[$dimension], 10);

    if ($intDim != $data[$dimension]) {
      form_set_error($dimension, t(ucfirst($dimension) . ' must be specified as a number in pixels.'));
    } elseif ($intDim < 1) {
      form_set_error($dimension, t(ucfirst($dimension) . ' must be a positive number of pixels.'));
    } elseif ($intDim > 10000) {
      form_set_error($dimension, t(ucfirst($dimension) . ' exceeds supported limit of 10,000px.'));
    }
  }
}

/**
 *
 * @param type $form
 * @param array $form_state
 */
function thirdlight_image_format_form_submit($form, &$form_state) {

  if (!empty($form_state['values']["fid"])) {
    db_delete('thirdlight_formats')
        ->condition('fid', $form_state['values']["fid"])
        ->execute();
  }

  $fields = array(
    "name" => $form_state['values']['name'],
    "width" => $form_state['values']['width'],
    "height" => $form_state['values']['height'],
    "format" => $form_state['values']['format'],
    "className" => $form_state['values']['className'],
  );

  db_insert('thirdlight_formats')
      ->fields($fields)
      ->execute();

  drupal_set_message(t('The output format "@format_name" was saved.', array('@format_name' => $form_state['values']['name'])));

  $form_state['redirect'] = 'admin/config/content/thirdlight/fields';
}

/**
 * Confirmation form for the deletion of an item.
 *
 * @param array $form_state
 *   The form.
 * @param array $form_state
 *   The form state (if any).
 * @param int $format_id
 *   The format ID to delete.
 *
 * @return array
 *   The form.
 */
function thirdlight_test_delete_confirm($form, $form_state, $format_id) {
  $format = thirdlight_load_formats($format_id);

  if (!isset($format[0])) {
    drupal_set_message(t('The output format could not be found.'), 'error');
    drupal_goto('admin/config/content/thirdlight/fields');
  }

  $format = array_pop($format);

  $form['fid'] = array(
    '#type' => 'hidden',
    '#value' => $format_id,
  );

  $title = t('Are you sure you want to delete the output format "@format_name"?', array('@format_name' => $format['name']));
  $description = t('Are you sure you want to delete the output format "@format_name"? This action cannot be undone.', array('@format_name' => $format['name']));

  $output = confirm_form($form, $title, isset($_GET['destination']) ? $_GET['destination'] : 'admin/config/content/thirdlight/fields', $description);

  return $output;
}

/**
 * Implements hook_submit().
 *
 * This is for the thirdlight_test_delete_confirm form.
 *
 * @param array $form
 *   The form.
 *
 * @param array $form_state
 *   The current state of the form.
 */
function thirdlight_test_delete_confirm_submit($form, &$form_state) {

  if ($form_state['values']['confirm']) {
    thirdlight_delete_format($form_state['values']['fid']);

    drupal_set_message(t('Format deleted.'));
    $form_state['redirect'] = 'admin/config/content/thirdlight/fields';
  }
}

/**
 * Callback for the test page, generates the needed JavaScript to run the thirdlight plugin.
 *
 * @return type
 */
function thirdlight_test_page() {

  thirdlight_load_js();

  $javascript = <<<'EOD'
(function($) {
  $(document).ready(function() {

    function thirdlight_clense_string(dirtystring) {
      if (dirtystring == '') {
        return '';
      }
      dirtystring = dirtystring.substring(0, 256).replace(/(<([^>]+)>)/ig, "").replace(/(\r\n|\n|\r)/gm, "");
      return dirtystring || '';
    }

    var thirdlight_config = Drupal.settings['thirdlight_config'];

    $(".thirdlight-button").click(function(event){
      event.stopPropagation();
      event.preventDefault();

      if (!thirdlight_config.configured) {
        if (thirdlight_config.reason) {
          alert(thirdlight_config.reason);
        } else {
          alert('The Third Light browser has not been configured yet');
        }
        return;
      }

      var app = new IMS.IframeAppOverlay(thirdlight_config.browserUrl, {
        options: thirdlight_config.options
      });

      app.on("cropChosen", function(cropDetails) {
        $('#thirdlight-image').attr('src', cropDetails.urlDetails.url);

        $('#thirdlight-image').attr('title', cropDetails.metadata[thirdlight_config.titleMode]);
        $('#thirdlight-image').attr('alt', cropDetails.metadata[thirdlight_config.titleMode]);

        $('#thirdlight-image').attr('width', cropDetails.urlDetails.width);
        $('#thirdlight-image').attr('height', cropDetails.urlDetails.height);

        if (thirdlight_config.titleMode && cropDetails.metadata) {
          if (cropDetails.metadata[thirdlight_config.titleMode] !== undefined) {
            $('input.thirdlight_preview_alt').val(thirdlight_clense_string(cropDetails.metadata[thirdlight_config.titleMode]));
          }

          if (cropDetails.metadata['headline'] !== undefined) {
            $('input.thirdlight_preview_title').val(thirdlight_clense_string(cropDetails.metadata['headline']));
          }
        }

        if (cropDetails.metadata['credit'] !== undefined) {
          $('input.thirdlight_preview_credit').val(thirdlight_clense_string(cropDetails.metadata['credit']));
        }
      });
    });
  });
})(jQuery);
EOD;

  drupal_add_js($javascript, array('type' => 'inline', 'scope' => 'footer'));

  // Generate a form for the test data to sit in.
  $thirdlight_test_form = array();
  $thirdlight_test_form['thirdlight_description'] = array(
    '#markup' => '<p>Use the following button and image to test your integration with Thirdlight.</p>'
  );

  $thirdlight_test_form['thirdlight_preview_alt'] = array(
    '#type' => 'textfield',
    '#title' => t('Alt text'),
    '#attributes' => array(
      'class' => array('thirdlight_preview_alt')
    )
  );

  $thirdlight_test_form['thirdlight_preview_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title text'),
    '#attributes' => array(
      'class' => array('thirdlight_preview_title')
    )
  );

  $thirdlight_test_form['thirdlight_preview_credit'] = array(
    '#type' => 'textfield',
    '#title' => t('Credit'),
    '#attributes' => array(
      'class' => array('thirdlight_preview_credit')
    )
  );  

  $thirdlight_test_form['thirdlight_preview'] = array(
    '#markup' => '<p><img id="thirdlight-image" src="" title="" alt="" width="150" height="150" /></p>'
  );

  $thirdlight_test_form['thirdlight_browse'] = array(
    '#type' => 'button',
    '#value' => 'Browse',
    '#attributes' => array(
      'class' => array('thirdlight-button')
    )
  );
  
  // As this is just a test form that we don't want to submit we just render it here.
  return render($thirdlight_test_form);
}
